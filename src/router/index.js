import Vue from 'vue'
import Router from 'vue-router'
import Shorter from '@/components/Shorter'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'Shorter',
            component: Shorter
        }
    ]
})
